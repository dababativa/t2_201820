package controller;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {
		
	}
	
	public static void loadTrips() {
		
	}
		
	public static DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		return null;
	}
	
	public static DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		return null;
	}
}
